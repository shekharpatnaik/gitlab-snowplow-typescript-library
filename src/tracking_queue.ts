export type SendEventCallback = (events: object[]) => Promise<void>;

enum TrackingQueueState {
  STARTED,
  STOPPING,
  STOPPED
}

export class TrackingQueue {

  private trackingQueue: object[] = [];

  private callback: SendEventCallback;

  private timeInterval: number;

  private maxItems: number;

  private currentState: TrackingQueueState;

  private timeout: NodeJS.Timeout | undefined;

  constructor(
    timeInterval: number,
    maxItems: number,
    callback: SendEventCallback,
  ) {
    this.maxItems = maxItems;
    this.timeInterval = timeInterval;
    this.callback = callback;
    this.currentState = TrackingQueueState.STOPPED;
  }

  async add(data: object) {
    this.trackingQueue.push(data);

    if (this.trackingQueue.length >= this.maxItems) {
      await this.drainQueue();
    }
  }

  private async drainQueue(): Promise<void> {
    if (this.trackingQueue.length > 0) {
      await this.callback(this.trackingQueue);
      this.trackingQueue = [];
    }
  }

  start() {
    this.timeout = setTimeout(async () => {
      await this.drainQueue();

      if (this.currentState !== TrackingQueueState.STOPPING) {
        this.start();
      }
    }, this.timeInterval)
    this.currentState = TrackingQueueState.STARTED;
  }

  async stop() {
    this.currentState = TrackingQueueState.STOPPING;
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = undefined;
    await this.drainQueue();
  }
}
