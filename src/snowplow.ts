import fetch from "cross-fetch";
import { v4 as uuidv4 } from 'uuid';
import { TrackingQueue } from './tracking_queue';

export type SnowplowOptions = {
  appId: string;
  endpoint: string;
  timeInterval: number;
  maxItems: number;
}

export class Snowplow {

  private trackingQueue: TrackingQueue;

  private options: SnowplowOptions;

  constructor(options: SnowplowOptions) {
    this.options = options;
    this.trackingQueue = new TrackingQueue(
      this.options.timeInterval,
      this.options.maxItems,
      this.sendEvent.bind(this)
    );
    this.trackingQueue.start();
  }

  private async sendEvent(events: object[]): Promise<void> {
    const url = `${this.options.endpoint}/com.snowplowanalytics.snowplow/tp2`;
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        "schema": "iglu:com.snowplowanalytics.snowplow/payload_data/jsonschema/1-0-4",
        "data": events.map(event => {

          const eventId = uuidv4();

          return {
            "e": "ue",
            "p": "app",
            "tv": "gitlab",
            "aid": this.options.appId,
            "eid": eventId,
            "ue_pr": JSON.stringify(event)
          }
        })
      })
    });

    if (response.status !== 200) {
      throw new Error(response.statusText)
    }
  }

  public async trackSelfDescribingEvent(event: object): Promise<void> {
    await this.trackingQueue.add(event);
  }

  async stop() {
    await this.trackingQueue.stop();
  }
}
