import { TrackingQueue } from "./tracking_queue";

describe('TrackingQueue', () => {

  describe('Tracking Queue interface', () => {

    it('should initialize', () => {
      const queue = new TrackingQueue(1000, 10, async () => {});
      expect(queue).not.toBeUndefined();
    });

    it('should add an event to the queue', async () => {
      const queue = new TrackingQueue(1000, 10, async () => {});
      await queue.add({});
    });

    it('should have a start and stop method', async () => {
      const queue = new TrackingQueue(1000, 10, async () => {});
      queue.start();
      await queue.stop();
    });
  });


  describe('Drain queue', () => {
    it('should drain when the max items is reached', async () => {
      const callback = jest.fn();
      const queue = new TrackingQueue(1000, 1, callback);
      queue.start();
      await queue.add({ data: 1 });
      expect(callback).toBeCalledWith([{ data: 1 }]);
      await queue.stop();
    });

    it('should drain when the time is reached', async () => {
      const callback = jest.fn();
      const queue = new TrackingQueue(500, 10, callback);
      queue.start();
      await queue.add({ data: 1 });
      await new Promise((r) => { setTimeout(r, 500) });
      expect(callback).toBeCalledWith([{ data: 1 }]);
      await queue.stop();
    });

    it('should not drain when the time has not been reached', async () => {
      const callback = jest.fn();
      const queue = new TrackingQueue(2000, 10, callback);
      queue.start();
      await queue.add({ data: 1 });
      await new Promise((r) => { setTimeout(r, 100) });
      expect(callback).not.toBeCalled();
      await queue.stop();
    });

    it('should drain when the queue is stopped', async () => {
      const callback = jest.fn();
      const queue = new TrackingQueue(2000, 10, callback);
      queue.start();
      await queue.add({ data: 1 });
      await new Promise((r) => { setTimeout(r, 100) });
      await queue.stop();
      expect(callback).toBeCalledWith([{ data: 1 }]);
    });

    it('should drain multiple items', async () => {
      const callback = jest.fn();
      const queue = new TrackingQueue(100, 10, callback);
      queue.start();
      await queue.add({ data: 1 });
      await queue.add({ data: 2 });
      await queue.stop();
      expect(callback).toBeCalledWith([{ data: 1 }, { data: 2 }]);
    });
  });
});
