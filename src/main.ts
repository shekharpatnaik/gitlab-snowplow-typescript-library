import { Snowplow } from "./snowplow";

async function main() {
  let snowplow = new Snowplow({
    maxItems: 5,
    timeInterval: 5000,
    endpoint: 'http://localhost:9091',
    appId: 'myapp'
  });

  await snowplow.trackSelfDescribingEvent({
      "schema":"iglu:com.snowplowanalytics.snowplow/unstruct_event/jsonschema/1-0-0",
      "data":{
          "schema":"iglu:com.snowplowanalytics.snowplow/link_click/jsonschema/1-0-1",
          "data":{
              "targetUrl":"https://gitlab.com/gitlab-org/gitlab-development-kit/-/settings/ci_cd",
              "elementId":"",
              "elementClasses":["nav-item-link","gl-rounded-base","gl-relative","gl-display-flex","gl-align-items-center","gl-min-h-7","gl-gap-3","gl-mb-1","gl-py-2","gl-text-black-normal!","gl-hover-bg-t-gray-a-08","gl-focus-bg-t-gray-a-08","gl-text-decoration-none!","gl-focus--focus","gl-px-3"],
              "elementTarget":""
          }
      }
  })

  await snowplow.trackSelfDescribingEvent({
      "schema":"iglu:com.snowplowanalytics.snowplow/unstruct_event/jsonschema/1-0-0",
      "data":{
          "schema":"iglu:com.snowplowanalytics.snowplow/link_click/jsonschema/1-0-1",
          "data":{
              "targetUrl":"https://gitlab.com/gitlab-org/gitlab-development-kit/-/settings/ci_cd",
              "elementId":"",
              "elementClasses":["nav-item-link","gl-rounded-base","gl-relative","gl-display-flex","gl-align-items-center","gl-min-h-7","gl-gap-3","gl-mb-1","gl-py-2","gl-text-black-normal!","gl-hover-bg-t-gray-a-08","gl-focus-bg-t-gray-a-08","gl-text-decoration-none!","gl-focus--focus","gl-px-3"],
              "elementTarget":""
          }
      }
  })

  snowplow.stop();
}

main()
  .catch(e => console.error(e))
