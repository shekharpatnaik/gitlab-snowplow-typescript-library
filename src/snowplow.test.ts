import fetch from "cross-fetch";
import { Snowplow, SnowplowOptions } from "./snowplow";

jest.mock('cross-fetch');

describe('Snowplow', () => {
  describe('Snowplow interface', () => {
    const options: SnowplowOptions = {
      appId: "test",
      timeInterval: 1000,
      maxItems: 1,
      endpoint: "http://localhost"
    }

    it('should initialize', async () => {
      const sp = new Snowplow(options);
      await sp.stop();
    });

    it('should let you track events', async () => {
      (fetch as jest.MockedFunction<typeof fetch>).mockResolvedValue({
              status: 200,
              statusText: "OK"
      } as Response);
      const sp = new Snowplow(options);
      await sp.trackSelfDescribingEvent({});
      await sp.stop();
    });

    it('should let you stop when the program ends', async () => {
      const sp = new Snowplow(options);
      await sp.stop();
    });
  });

  describe('should track and send events to snowplow', () => {
    let sp: Snowplow;

    beforeEach(() => {
      (fetch as jest.MockedFunction<typeof fetch>).mockClear();

      sp = new Snowplow({
        appId: 'testApp',
        endpoint: 'https://example.com',
        timeInterval: 3000,
        maxItems: 1,
      });
    });

    afterEach(async () => {
      await sp.stop();
    });

    it('should send the events to snowplow', async () => {
      (fetch as jest.MockedFunction<typeof fetch>).mockResolvedValue({
        status: 200,
        statusText: "OK"
      } as Response);

      await sp.trackSelfDescribingEvent({ data: 1 });
      await sp.trackSelfDescribingEvent({ data: 2 });
      expect(fetch).toBeCalledTimes(2);
      await sp.stop();
    });
  });
});
